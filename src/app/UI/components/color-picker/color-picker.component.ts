import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

const pallet_10 = [
  ['500', '#faeb3b'],
  ['50', '#fefde7'],
  ['100', '#fef9c4'],
  ['200', '#fdf59d'],
  ['300', '#fcf176'],
  ['400', '#fbee58'],
  ['600', '#f9e935'],
  ['700', '#f9e52d'],
  ['800', '#f8e226'],
  ['900', '#f6dd19'],
  ['A100', '#ffffff'],
  ['A200', '#fffef5'],
  ['A400', '#fff7c2'],
  ['A700', '#fff4a8'],
]
const pallet_9 = [
  ['500', '#269688'],
  ['50', '#e5f2f1'],
  ['100', '#bee0db'],
  ['200', '#93cbc4'],
  ['300', '#67b6ac'],
  ['400', '#47a69a'],
  ['600', '#228e80'],
  ['700', '#1c8375'],
  ['800', '#17796b'],
  ['900', '#0d6858'],
  ['A100', '#9cffec'],
  ['A200', '#69ffe2'],
  ['A400', '#36ffd8'],
  ['A700', '#1cffd3'],
]
const pallet_8 = [
  ['500', '#3abcd4'],
  ['50', '#e7f7fa'],
  ['100', '#c4ebf2'],
  ['200', '#9ddeea'],
  ['300', '#75d0e1'],
  ['400', '#58c6da'],
  ['600', '#34b6cf'],
  ['700', '#2cadc9'],
  ['800', '#25a5c3'],
  ['900', '#1897b9'],
  ['A100', '#edfbff'],
  ['A200', '#baefff'],
  ['A400', '#87e3ff'],
  ['A700', '#6eddff'],
]
const pallet_7 = [
  ['500', '#40a9f4'],
  ['50', '#e8f5fe'],
  ['100', '#c6e5fc'],
  ['200', '#a0d4fa'],
  ['300', '#79c3f7'],
  ['400', '#5db6f6'],
  ['600', '#3aa2f3'],
  ['700', '#3298f1'],
  ['800', '#2a8fef'],
  ['900', '#1c7eec'],
  ['A100', '#ffffff'],
  ['A200', '#edf5ff'],
  ['A400', '#bad9ff'],
  ['A700', '#a1caff'],
]
const pallet_6 = [
  ['500', '#3e96f3'],
  ['50', '#e8f2fe'],
  ['100', '#c5e0fb'],
  ['200', '#9fcbf9'],
  ['300', '#78b6f7'],
  ['400', '#5ba6f5'],
  ['600', '#388ef1'],
  ['700', '#3083ef'],
  ['800', '#2879ed'],
  ['900', '#1b68ea'],
  ['A100', '#ffffff'],
  ['A200', '#ebf1ff'],
  ['A400', '#b8d0ff'],
  ['A700', '#9ebfff'],
]
const pallet_5 = [
  ['500', '#673ab7'],
  ['50', '#ede7f6'],
  ['100', '#d1c4e9'],
  ['200', '#b39ddb'],
  ['300', '#9575cd'],
  ['400', '#7e58c2'],
  ['600', '#5f34b0'],
  ['700', '#542ca7'],
  ['800', '#4a259f'],
  ['900', '#391890'],
  ['A100', '#d4c7ff'],
  ['A200', '#ad94ff'],
  ['A400', '#8661ff'],
  ['A700', '#7347ff'],
]
const pallet_4 = [
  ['500', '#9c27b0'],
  ['50', '#f3e5f6'],
  ['100', '#e1bee7'],
  ['200', '#ce93d8'],
  ['300', '#ba68c8'],
  ['400', '#ab47bc'],
  ['600', '#9423a9'],
  ['700', '#8a1da0'],
  ['800', '#801797'],
  ['900', '#6e0e87'],
  ['A100', '#efb8ff'],
  ['A200', '#e485ff'],
  ['A400', '#d852ff'],
  ['A700', '#d238ff'],
]
const pallet_3 = [
  ['500', '#e91e62'],
  ['50', '#fce4ec'],
  ['100', '#f8bcd0'],
  ['200', '#f48fb1'],
  ['300', '#f06291'],
  ['400', '#ec407a'],
  ['600', '#e61a5a'],
  ['700', '#e31650'],
  ['800', '#df1246'],
  ['900', '#d90a34'],
  ['A100', '#ffffff'],
  ['A200', '#ffd0d8'],
  ['A400', '#ff9dad'],
  ['A700', '#ff8498'],
]
const pallet_2 = [
  ['500', '#f44336'],
  ['50', '#fee8e7'],
  ['100', '#fcc7c3'],
  ['200', '#faa19b'],
  ['300', '#f77b72'],
  ['400', '#f65f54'],
  ['600', '#f33d30'],
  ['700', '#f13429'],
  ['800', '#ef2c22'],
  ['900', '#ec1e16'],
  ['A100', '#ffffff'],
  ['A200', '#ffe9e9'],
  ['A400', '#ffb8b6'],
  ['A700', '#ff9f9c'],
]
const pallet_1 = [
  ['500', '#000000'],
  ['50', '#e0e0e0'],
  ['100', '#b3b3b3'],
  ['200', '#808080'],
  ['300', '#4d4d4d'],
  ['400', '#262626'],
  ['600', '#000000'],
  ['700', '#000000'],
  ['800', '#000000'],
  ['900', '#000000'],
  ['A100', '#a6a6a6'],
  ['A200', '#8c8c8c'],
  ['A400', '#737373'],
  ['A700', '#666666'],
]

const pallets = [
  pallet_1,
  pallet_2,
  pallet_3,
  pallet_4,
  pallet_5,
  pallet_6,
  pallet_7,
  pallet_8,
  pallet_9,
  pallet_10,
]

const COLOR_LIST_WIDTH = 268
@Component({
  selector: 'app-color-picker',
  templateUrl: './color-picker.component.html',
  styleUrls: ['./color-picker.component.scss']
})
export class ColorPickerComponent implements OnInit {
  @Input() color: string = 'black'
  @Input() label: string = ''
  @Output() selectedColor: EventEmitter<string> = new EventEmitter<string>();
  isShowMenu = false;
  pallets = pallets

  get colorBoxHeight(): number {
    return COLOR_LIST_WIDTH / pallets.length
  };

  constructor() { }

  ngOnInit(): void {
  }

  toggleSelectMenu() {
    this.isShowMenu = !this.isShowMenu
  }

  setColor(color: string) {
    this.selectedColor.next(color)
    this.color = color
    this.isShowMenu = false
  }
}
