import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {FormControl} from "@angular/forms";
import {debounceTime, distinctUntilChanged, Subject, takeUntil} from "rxjs";

@Component({
  selector: 'app-input[id][placeholder]',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss']
})
export class InputComponent implements OnInit, OnDestroy {
  private _unsubscribe$ = new Subject<null>();
  @Input() id: string = ''
  @Input() isRequired: boolean = false
  @Input() placeholder: string = ''
  @Input() label: string = ''
  @Input() control: FormControl = new FormControl('');
  @Input() debounceTimeMs: number = 0

  @Output() inputValue: EventEmitter<string> = new EventEmitter<string>();

  constructor() { }

  ngOnInit(): void {
    this.control.valueChanges.pipe(
      takeUntil(this._unsubscribe$),
      distinctUntilChanged(),
      debounceTime(this.debounceTimeMs)
    ).subscribe(res => {
      this.inputValue.emit(res)
    })
  }

  ngOnDestroy(): void {
    this._unsubscribe$.next(null);
    this._unsubscribe$.complete();
  }

}
