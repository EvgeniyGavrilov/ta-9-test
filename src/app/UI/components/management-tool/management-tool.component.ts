import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ViewMode} from "@types";

@Component({
  selector: 'app-management-tool[activeViewMode]',
  templateUrl: './management-tool.component.html',
  styleUrls: ['./management-tool.component.scss']
})
export class ManagementToolComponent implements OnInit {
  @Input() activeViewMode: ViewMode | undefined;
  @Output() viewMode: EventEmitter<ViewMode> = new EventEmitter<ViewMode>()
  @Output() toggleUserMenu: EventEmitter<void> = new EventEmitter<void>()
  constructor() { }

  ngOnInit(): void {
  }

  setViewType(viewMode: ViewMode) {
    this.viewMode.next(viewMode)
  }
}
