import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Subject, takeUntil} from "rxjs";

export type StartElementIndex = number
export type EndElementIndex = number
export type Range = [StartElementIndex, EndElementIndex]

@Component({
  selector: 'app-paginator',
  templateUrl: './paginator.component.html',
  styleUrls: ['./paginator.component.scss']
})
export class PaginatorComponent implements OnInit, OnDestroy {
  private _unsubscribe$ = new Subject<null>()
  private _length = 0;
  private _pageIndex = 0;
  private _pages = 0;
  private _pageSize = 0;
  pagesArray = [0]
  get pages(): number {
    return this._pages;
  }
  get pageSize(): number {
    return this._pageSize;
  }
  get pageIndex(): number {
    return this._pageIndex;
  }
  size: FormControl<number> = new FormControl<number>(0, {nonNullable: true})
  page: FormControl<number> = new FormControl<number>(0, {nonNullable: true})
  @Input() set length(n: number) {
    this._length = n;
    if (this._pageSize) {
      this.calcPages();
    }
  };
  @Input() set pageSize(n: number) {
    this._pageSize = n;
    this.size.patchValue(this._pageSize);
    if(this._length) {
      this.calcPages();
    }
  };
  @Input() pageSizeOptions: number[] = [5, 10, 15];
  @Output() range: EventEmitter<Range> = new EventEmitter<Range>();

  constructor() { }

  ngOnInit(): void {
    this.size.valueChanges
      .pipe(
        takeUntil(this._unsubscribe$)
      )
      .subscribe(size => {
        this._pageSize = size;
        if(this._length) {
          this.calcPages();
        }
      })

    this.page.valueChanges
      .pipe(
        takeUntil(this._unsubscribe$)
      )
      .subscribe(pageNum => {
        this.goToPageByIndex(pageNum)
      })
  }

  calcPages() {
    this._pages = this._length === 0 ? 1 : Math.ceil(this._length / this._pageSize);
    if(this._pages === 1 ) {
      this._pageIndex = 0
    } else if(this._pages === this._pageIndex || this._pages <= this._pageIndex) {
      this._pageIndex = this._pages - 1
    }
    this.calcRange();
  }

  calcRange() {
    if(this.pages) {
      this.pagesArray = [];
      for(let i = 0; i < this.pages; i++) {
        this.pagesArray.push(i)
      }
    } else {
      this.pagesArray = [-1];
    }
    console.log(this.pages);
    this.page.patchValue(this._pageIndex)
    console.log('_pageIndex: ' + this._pageIndex)
    console.log('this.pagesArray');
    console.log(this.pagesArray);
    console.log(this.pages);
    const startElIdx = this._pageIndex === 1 && this._pageSize === 1 ? 0 : this._pageSize * this._pageIndex;
    const endElIdx = startElIdx + this._pageSize <= this._length ? startElIdx + this._pageSize : this._length;




    this.range.emit([startElIdx, endElIdx])
  }

  goToNextPage() {
    if(this._pageIndex + 1 < this._pages) {
      ++this._pageIndex
      this.calcRange()
    }
  }

  goToPrevPage() {
    if(this._pageIndex - 1 >= 0) {
      --this._pageIndex
      this.calcRange()
    }
  }

  goToPageByIndex(number: number | null) {
    if(this._pageIndex !== number && number !== null) {
      this._pageIndex = number;
      this.calcRange()
    }
  }

  get pagesForDisplay(): number {
    return this.pageIndex ? this.pageIndex + 1 : 1
  }

  ngOnDestroy(): void {
    this._unsubscribe$.next(null);
    this._unsubscribe$.complete();
  }
}
