import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'paginationButton',
})
export class PaginationButtonPipe implements PipeTransform {

  transform(value: Array<number | null>, currentPageIndex: number, siblingElCounter: number = 1, boundaryElCounter: number = 1): Array<number | null> {
    const pagesArr = value.map(e => e === null ? e : Math.abs(e));
    const boundary = Math.abs(boundaryElCounter);
    const intermediateButton = 1;
    const sibling = Math.abs(siblingElCounter);

    const boundaries = boundary * 2;
    const intermediateButtons = intermediateButton * 2;
    const siblings = sibling * 2;
    const selectedBtd = 1;

    const minBtnCounterForRenderDot = boundaries + intermediateButtons + siblings + selectedBtd;

    if (pagesArr.length <= minBtnCounterForRenderDot) {
      return pagesArr;
    }

    const leftGap = currentPageIndex - sibling - boundary;
    const rightGap = (pagesArr.length - 1) - currentPageIndex - sibling - boundary;

    if (leftGap === 1 && rightGap === 1) {
      // console.log('i selected i')
      return pagesArr;
    }

    if (leftGap === 1 && rightGap > 1) {
      // console.log('i selected ...')
      const left = pagesArr.slice(0, currentPageIndex + selectedBtd + sibling);
      const right = pagesArr.slice(-boundary);
      return [...left, null, ...right];
    }

    if (leftGap > 1 && rightGap === 1) {
      // console.log('... selected i')
      const left = pagesArr.slice(0, boundary);
      const right = pagesArr.slice(-(sibling + selectedBtd + sibling + intermediateButton + boundary));
      return [...left, null, ...right];
    }

    if (leftGap > 1 && rightGap > 1) {
      // console.log('... selected ...')
      const left = pagesArr.slice(0, boundary);
      const center = pagesArr.slice(boundary + leftGap, boundary + leftGap + siblings + selectedBtd);
      const right = pagesArr.slice(-boundary);
      return [...left, null, ...center, null, ...right];
    }

    if (rightGap > 1) {
      // console.log('selected ...')
      const left = pagesArr.slice(0, boundary + intermediateButton + sibling + selectedBtd + sibling);
      const right = pagesArr.slice(-boundary);
      return [...left, null, ...right];
    }

    if (leftGap > 1) {
      // console.log('... selected')
      const left = pagesArr.slice(0, boundary);
      const right = pagesArr.slice(-(boundary + intermediateButton + sibling + selectedBtd + sibling));
      return [...left, null, ...right];
    }
    console.error('Unexpected case')
    return pagesArr;
  }

}
