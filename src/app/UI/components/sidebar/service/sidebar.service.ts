import { Injectable } from '@angular/core';
import {Router} from "@angular/router";
import {Item} from "@classes";

@Injectable({
  providedIn: 'root'
})
export class SidebarService {
  private _isShowSideBar: boolean = false;
  private _item: Item = new Item();
  constructor(private router: Router) { }

  get isShowSideBar() {
    return this._isShowSideBar
  }
  set isShowSideBar(v: boolean) {
    this._isShowSideBar = v
    if(!v) {
      this.router.navigate(['/'])
    }
  }

  set item(v: Item) {
    this._item = v
  }
  get item(): Item {
    return this._item
  }
}
