import { Component, OnInit } from '@angular/core';
import {SidebarService} from "./service/sidebar.service";
import {Item} from "../../../common/classes/Item";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {DataService} from "@services";


@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  item: Item;
  itemForm : FormGroup;
  constructor(
    private fb: FormBuilder,
    private sidebarService: SidebarService,
    private dataService: DataService
  ) {
    this.item = this.sidebarService.item;
    this.itemForm = this.fb.group({
      name: [this.item.name, [Validators.required]],
      color: [this.item.color],
      description: [this.item.description]
    })
  }

  ngOnInit(): void {
  }

  close($event?: MouseEvent) {
    $event?.stopPropagation();
    $event?.preventDefault();
    this.sidebarService.item = new Item();
    this.sidebarService.isShowSideBar = false
  }

  getFormControlByName(name: string): FormControl {
    return this.itemForm.get(name) as FormControl
  }

  save($event: MouseEvent) {
    $event.stopPropagation();
    $event.preventDefault();

    const name = this.itemForm.value['name'];
    const color = this.itemForm.value['color'];
    const description = this.itemForm.value['description'];
    this.item.name = name;
    this.item.color = color;
    this.item.description = description;
    const lastUpdate = Date.now();
    this.item.last_update = lastUpdate;
    if(this.item.id === undefined) {
      this.item.id = this.dataService.items.length;
      this.item.created_by = this.dataService.user.name;
      this.item.create_date = lastUpdate;
      // this.dataService.items.push(this.item)
      this.dataService.addNewItem(this.item)
    }
    this.close()

  }

  setColor(color: string) {
    this.itemForm.get('color')?.patchValue(color);
    // this.item.color = color
  }
}
