import {Component, Input, OnInit} from '@angular/core';
import {Item} from "@classes";

export type HeadersConf = {
  prop_name: keyof Item,
  header_name: string,
  withSize?: string
}

@Component({
  selector: 'app-table[itemList][headers]',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {
  @Input() itemList: Item[] = [];
  @Input() headers: HeadersConf[] = []
  constructor() { }

  ngOnInit(): void {}

  getMinWidth() {
    return 100 / this.headers.length
  }
}
