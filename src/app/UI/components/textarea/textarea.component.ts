import {Component, Input, OnInit} from '@angular/core';
import {FormControl} from "@angular/forms";

@Component({
  selector: 'app-textarea[id][placeholder][control][resize]',
  templateUrl: './textarea.component.html',
  styleUrls: ['./textarea.component.scss']
})
export class TextareaComponent implements OnInit {
  @Input() id: string = ''
  @Input() placeholder: string = ''
  @Input() label: string = ''
  @Input() resize: boolean = false
  @Input() control: FormControl = new FormControl('');
  constructor() { }

  ngOnInit(): void {
  }

}
