import {Component, Input, OnInit} from '@angular/core';
import {Item} from "@classes";

@Component({
  selector: 'app-tile[itemList]',
  templateUrl: './tile.component.html',
  styleUrls: ['./tile.component.scss']
})
export class TileComponent implements OnInit {
  @Input() itemList: Item[] = [];
  constructor() { }

  ngOnInit(): void {
  }

}
