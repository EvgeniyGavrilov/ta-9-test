import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { PaginatorComponent } from './components/paginator/paginator.component';
import { ColorPickerComponent } from './components/color-picker/color-picker.component';
import { TableComponent } from './components/table/table.component';
import { TileComponent } from './components/tile/tile.component';
import {RouterModule} from "@angular/router";
import { ManagementToolComponent } from './components/management-tool/management-tool.component';
import {ReactiveFormsModule} from "@angular/forms";
import { InputComponent } from './components/input/input.component';
import { ButtonComponent } from './components/button/button.component';
import { TextareaComponent } from './components/textarea/textarea.component';
import { PaginationButtonPipe } from './components/paginator/pipe/pagination-button.pipe';


@NgModule({
  declarations: [
    SidebarComponent,
    PaginatorComponent,
    ColorPickerComponent,
    TableComponent,
    TileComponent,
    ManagementToolComponent,
    InputComponent,
    ButtonComponent,
    TextareaComponent,
    PaginationButtonPipe,
  ],
  exports: [
    SidebarComponent,
    TableComponent,
    TileComponent,
    ManagementToolComponent,
    PaginatorComponent,
    InputComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    ReactiveFormsModule
  ]
})
export class UIModule { }
