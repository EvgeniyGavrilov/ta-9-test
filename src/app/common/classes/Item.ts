export type UserId = number;
export type Timestamp = number;

export class Item {
  private _id?: number;
  private _color: string = 'black';
  private _name: string = '';
  private _description: string = '';
  private _create_date: Timestamp | null = null;
  private _last_update: Timestamp | null = null;
  // private _created_by: UserId | null = null;
  private _created_by: string = '';

  constructor()
  constructor(
    id?: number,
    color: string = 'black',
    name: string = '',
    description: string = '',
    create_date: number | null = null,
    last_update: number | null = null,
    // created_by: number | null = null
    created_by: string = ''
  )
  {
    this._id = id;
    this._color = color;
    this._name = name;
    this._description = description;
    this._create_date = create_date;
    this._last_update = last_update;
    this._created_by = created_by;
  }

  get id() {
    return this._id;
  }
  set id(v) {
    this._id = v;
  }

  get color() {
    return this._color;
  }
  set color(v) {
    this._color = v;
  }

  get name() {
    return this._name;
  }
  set name(v) {
    this._name = v;
  }

  get description() {
    return this._description;
  }
  set description(v) {
    this._description = v;
  }

  get create_date() {
    return this._create_date;
  }
  set create_date(v) {
    this._create_date = v;
  }

  get last_update() {
    return this._last_update;
  }
  set last_update(v) {
    this._last_update = v;
  }

  get created_by() {
    return this._created_by;
  }
  set created_by(v) {
    this._created_by = v;
  }
}
