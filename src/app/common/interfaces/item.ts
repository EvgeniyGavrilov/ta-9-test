export interface Item {
  id?: number,
  color: string,
  name: string | null,
  create_date: number | null,
  last_update: number | null,
  created_by: number | null
}
