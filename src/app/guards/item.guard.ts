import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
  UrlTree
} from '@angular/router';
import { Observable } from 'rxjs';
import {DataService} from "@services";
import {SidebarService} from "@UI";
import {Item} from "@classes";

@Injectable({
  providedIn: 'root'
})
export class ItemGuard implements CanActivate {
  constructor(
    private dataService: DataService,
    private router: Router,
    private sidebarService: SidebarService,
  ) {
  }
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    const el = this.dataService.items.find(el => el.id === Number(route.params['id']))
    if (!!el) {
      this.sidebarService.isShowSideBar = true;
      this.sidebarService.item = el;
    } else {
      this.sidebarService.isShowSideBar = false;
      this.sidebarService.item = new Item();
    }
    return !!el ? true  : this.router.navigate(['/']);

  }

}
