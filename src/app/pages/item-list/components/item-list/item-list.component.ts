import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subject, takeUntil} from "rxjs";
import {DataService} from "@services";
import {ViewMode} from "@types";
import {Item} from "@classes";
import {SidebarService, HeadersConf, Range} from "@UI";


@Component({
  selector: 'app-item-tile',
  templateUrl: './item-list.component.html',
  styleUrls: ['./item-list.component.scss']
})
export class ItemListComponent implements OnInit, OnDestroy {
  private _unsubscribe$: Subject<null> = new Subject<null>();
  private filterByNameValue: string = '';
  viewMode: ViewMode = "tile";
  itemListPreFilter: Item[] = [];
  itemList: Item[] = [];
  headers: HeadersConf[] = [
    {
      prop_name: 'color',
      header_name: 'Color'
    },
    {
      prop_name: 'name',
      header_name: 'Name'
    },
    {
      prop_name: 'create_date',
      header_name: 'Create Date'
    },
    {
      prop_name: 'last_update',
      header_name: 'Last Update'
    },
    {
      prop_name: 'created_by',
      header_name: 'Created By'
    }
  ];

  constructor(private dataService: DataService, private sidebarService: SidebarService) {
    this.dataService.getData().pipe(
      takeUntil(this._unsubscribe$)
    ).subscribe(res => {
      this.itemListPreFilter = this.filterByName();
      this.itemList = this.itemListPreFilter;
    })
  }

  ngOnInit(): void {
  }

  setViewMode($event: ViewMode) {
    this.viewMode = $event
  }

  toggleUserMenu() {
    this.sidebarService.isShowSideBar = true
  }

  get isShowSideBar(): boolean {
    return this.sidebarService.isShowSideBar
  }

  setFilterInputValue(name: string) {
    this.filterByNameValue = name.trim().toLowerCase();
    this.itemListPreFilter = this.filterByName();
    this.itemList = this.itemListPreFilter;
  }

  filterByName(): Item[] {
    if(this.filterByNameValue) {
      return this.dataService.items.filter(el => el.name.trim().toLowerCase().includes(this.filterByNameValue))
    } else {
      return this.dataService.items;
    }
  }

  filterByRange(range: Range = [0, 0]) {
    this.itemList = this.itemListPreFilter.slice(range[0], range[1])
  }

  ngOnDestroy(): void {
    this._unsubscribe$.next(null);
    this._unsubscribe$.complete();
  }
}
