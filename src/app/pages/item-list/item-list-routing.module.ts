import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ItemListComponent} from "./components/item-list/item-list.component";
import {ItemGuard} from "@guards";


const routes: Routes = [
  {
    path: '',
    component: ItemListComponent,
    children: [
      {
        path: ':id',
        component: ItemListComponent,
        canActivate: [ItemGuard]

      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ItemListRoutingModule { }
