import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {map, Observable, ReplaySubject, take, tap} from "rxjs";
import {Item} from "../common/classes/Item";

export interface User {
  id: number,
  name: string
}

@Injectable({
  providedIn: 'root'
})
export class DataService {
  private _user: User = {
    id: 0,
    name: 'Admin'
  }
  private _items: Item[] = [];
  private _items$: ReplaySubject<Item[]> = new ReplaySubject<Item[]>(1);

  constructor(private http: HttpClient) {
  }

  getData(): Observable<Item[]>  {
    this.http.get<{items: Item[]}>('../../assets/data.json').pipe(
      take(1),
      map(res => res.items),
      tap(res => {
        this._items = res
      })
    ).subscribe( res => this._items$.next(res))

    return this._items$
  }

  get items(): Item[] {
    return this._items
  }

  get user() {
    return this._user
  }

  addNewItem(item: Item) {
    this._items.push(item);
    this._items$.next(this._items)
  }
}
